# symfony/[contracts](https://packagist.org/packages/symfony/contracts)

A set of abstractions extracted out of the Symfony components. https://symfony.com/doc/current/components/contracts.html

## Official documentation
* [*Symfony Contracts, battle-tested semantics you can depend on*
  ](https://symfony.com/blog/symfony-contracts-battle-tested-semantics-you-can-depend-on)
  2019 Javier Eguiluz